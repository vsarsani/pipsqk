#!/usr/bin/env python

import pickle
import unittest
import tempfile

from pipsqk.components import SuperPipe, Pipeline, Segment, Step

class SpecialSegment(Pipeline):
    def prepare(self):
        return 'verify_subclass'

class TestPickle(unittest.TestCase):
    
    def testSaveAndReload(self):
        tempDir = tempfile.mkdtemp()

        stepA = Step(name='stepA')
        stepB = Step(name='stepB')
        stepB.after(stepA)
        subsegmentA = Segment([stepA, stepB])
        segmentA = Pipeline(
            [subsegmentA],
            directory=tempDir,
            name='segmentA'
        )
        pipeline = SuperPipe(
            [segmentA],
            directory=tempDir,
            name='testPipeline'
        )

        def fxn(self):
            return "verify_prepareFunction"

        segmentA.setPrepareFunction(fxn)

        self.assertEqual(
            segmentA.prepare(),
            'verify_prepareFunction')

        filename = pipeline._getPipsqkFile()
        pipeline.save()
        with open(filename,'rb') as f:
            reloadedPipeline = pickle.load(f)

        # spot-check to verify same as pre-save
        self.assertEqual(len(reloadedPipeline._children), 1)
        reloadedSegment = reloadedPipeline._children[0]
        reloadedSegment._flatten()
        first = reloadedSegment._getFirstToRun()[0]
        last = reloadedSegment._getLastToRun()[0]
        self.assertEqual(first.getAttribute('name'), 'stepA')
        self.assertEqual(last.getAttribute('name'), 'stepB')

        self.assertEqual(
            reloadedSegment.prepare(),
            'verify_prepareFunction')

    def testSubclassedSegment(self):
        tempDir = tempfile.mkdtemp()

        segmentA = SpecialSegment(
            directory=tempDir,
            name='segmentA'
        )
        pipeline = SuperPipe(
            [segmentA],
            directory=tempDir,
            name='testPipeline'
        )

        filename = pipeline._getPipsqkFile()
        pipeline.save()
        with open(filename,'rb') as f:
            reloadedPipeline = pickle.load(f)
        
        reloadedSegment = reloadedPipeline._children[0]

        self.assertEqual(
            'verify_subclass', 
            reloadedSegment.prepare())

if __name__ == '__main__':
    unittest.main()
