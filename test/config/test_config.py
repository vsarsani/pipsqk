#!/usr/bin/env python

import os
import unittest
import warnings
import pipsqk

class TestConfig(unittest.TestCase):

    def setUp(self):
        self.restoreRackEnv = os.getenv('RACK_ENV')
        os.environ['RACK_ENV'] = ''

    def tearDown(self):
        if self.restoreRackEnv:
            os.environ['RACK_ENV'] = self.restoreRackEnv
        else:
            os.unsetenv('RACK_ENV')

    def test_set_rack_env_with_variable(self):
        from testconfig1 import config
        env = 'env1'
        os.environ['RACK_ENV'] = env
        config = config.Config()
        self.assertEqual(config.rackEnvironment, env)
        self.assertEqual(config.get('setting2'),env)

    def test_set_rack_env_with_file(self):
        from testconfig2 import config
        config = config.Config()
        self.assertEqual(config.rackEnvironment, 'env2')
        self.assertEqual(config.get('setting2'),'env2')

    def test_override_variable_with_file(self):
        from testconfig2 import config
        os.environ['RACK_ENV'] = 'env1'
        config = config.Config()
        self.assertEqual(config.rackEnvironment, 'env2')
        self.assertEqual(config.get('setting2'),'env2')

    def test_only_one_env_defined(self):
        # should default to only available if not specified
        from testconfig3 import config
        config = config.Config()
        self.assertEqual(config.rackEnvironment, 'env1')
        self.assertEqual(config.get('setting2'),'env1')

    def test_default_settings(self):
        from testconfig4 import config
        config = config.Config()
        self.assertEqual(config.rackEnvironment, 'env2')
        self.assertEqual(config.get('setting1'), 'default')

    def test_empty_config(self):
        from testconfig5 import config
        with warnings.catch_warnings(record=True) as w:
            config = config.Config()
            self.assertEqual(len(w),1)
        self.assertEqual(config.rackEnvironment, None)

if __name__ == '__main__':
    unittest.main()
