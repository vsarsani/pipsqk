#!/usr/bin/env python

import os
import tempfile
import unittest

from pipsqk.components import Step, Segment, Pipeline, SuperPipe
from pipsqk.job_managers.sjm import SJMJobManager
from pipsqk.job_managers.bpipe import BpipeJobManager


# For use in tests that require subclassing Pipeline

class ChildOfSegment(Pipeline):

    def prepare(self):
        A = Step(name='A')
        B = Step(name='B')
        B.after(A)
        self.add(Segment([A, B]))

        return 2.72 # Simple way to test that prepare() was called

class TestComponents(unittest.TestCase):

    # - Helper class ------------------------------

    def testNameGenerator(self):
        # Two unnamed steps should have different non-null names
        A = Step()
        B = Step()
        self.assertIsNotNone(A.getAttribute('name'))
        self.assertIsNotNone(B.getAttribute('name'))
        self.assertNotEqual(A.getAttribute('name'),
                            B.getAttribute('name'))

    # _OrderableComponent class --------------------

    def testAfter(self):
        stepA = Step()
        stepB = Step()
        stepB.after(stepA)
        self.assertTrue(stepB._isAfter(stepA))
        self.assertFalse(stepA._isAfter(stepB))

        stepC = Step()
        subsegmentA = Segment()
        subsegmentB = Segment()
        subsegmentB.after(stepC)
        subsegmentB.after(subsegmentA)
        self.assertTrue(subsegmentB._isAfter(stepC))
        self.assertTrue(subsegmentB._isAfter(subsegmentA))
        self.assertFalse(subsegmentA._isAfter(subsegmentB))
        self.assertFalse(stepC._isAfter(subsegmentB))

    def testBefore(self):
        stepA = Step()
        stepB = Step()
        stepA.before(stepB)
        self.assertTrue(stepA._isBefore(stepB))
        self.assertFalse(stepB._isBefore(stepA))

        stepC = Step()
        subsegmentA = Segment()
        subsegmentB = Segment()
        stepC.before(subsegmentB)
        subsegmentA.before(subsegmentB)
        self.assertTrue(subsegmentB._isAfter(stepC))
        self.assertTrue(subsegmentB._isAfter(subsegmentA))
        self.assertFalse(subsegmentA._isAfter(subsegmentB))
        self.assertFalse(stepC._isAfter(subsegmentB))

    def testUnAfter(self):
        stepA = Step()
        stepB = Step()
        stepA.after(stepB)
        stepA._unAfter(stepB)
        self.assertFalse(stepA._isAfter(stepB))
        self.assertRaises(Exception,
                          stepA._unAfter,
                          stepB)

    def testUnBefore(self):
        stepA = Step()
        stepB = Step()
        stepB.after(stepA)
        stepA._unBefore(stepB)
        self.assertFalse(stepB._isAfter(stepA))
        self.assertRaises(Exception,
                          stepA._unBefore,
                          stepB)

    def testAfterList(self):
        A = Step()
        B = Segment()
        C = Step()

        C.afterList([A, B])
        self.assertTrue(C._isAfter(A))
        self.assertTrue(C._isAfter(B))

        C._unAfterList([A, B])
        self.assertFalse(C._isAfter(A))
        self.assertFalse(C._isAfter(B))

        self.assertRaises(Exception,
                          C._unAfterList,
                          [A, B])

    def testBeforeList(self):
        A = Segment()
        B = Segment()
        C = Step()

        A.beforeList([B, C])
        self.assertTrue(A._isBefore(B))
        self.assertTrue(A._isBefore(C))
        
        A._unBeforeList([B, C])
        self.assertFalse(A._isBefore(B))
        self.assertFalse(A._isBefore(C))

        self.assertRaises(Exception,
                          A._unBeforeList,
                          [B, C])

    def testGetPending(self):
        A = Segment()
        B = Segment()
        C = Segment()
        C.after(B)
        A.before(C)
        self.assertTrue(A in C._getPrerequisite())
        self.assertTrue(B in C._getPrerequisite())

    def testGetPending(self):
        A = Step()
        B = Step()
        C = Step()
        A.before(B)
        C.after(A)
        self.assertTrue(B in A._getPending())
        self.assertTrue(C in A._getPending())

    # _ContainableComponent class -----------------------------------

    def testIsContainable(self):
        A = Segment()
        self.assertTrue(A._isContainable)

    def testSetGetRemoveParent(self):
        A = Segment()
        B = Segment()
        C = Segment()

        A._setParent(B)
        self.assertTrue(A in B._children)

        # Can't have two parents
        self.assertRaises(
            Exception,
            A._setParent,
            C )

        self.assertEqual(A._getParent(), B)
        A._removeParent()

        self.assertEqual(A._getParent(), None)
        A._setParent(C)

        self.assertEqual(A._getParent(), C)

    def testGetPipeline(self):
        A = Step()
        B = Segment()
        C = Pipeline()
        B.add(A)
        C.add(B)

        self.assertEqual(A._getPipeline(), C)
        self.assertEqual(B._getPipeline(), C)

    # _ContainerComponent class -------------------------------------

    def testComponentsInPipeline(self):
        segmentA = Pipeline()
        segmentA.add(Step())
        segmentA.add(Segment())
        self.assertEqual(len(segmentA._children), 2)

        segmentB = Pipeline()
        step = segmentB.newStep(name='step')
        subsegment = segmentB.newSegment([Step()])

        self.assertEqual(step.getAttribute('name'), 'step')
        self.assertEqual(len(subsegment._children), 1)
        self.assertEqual(len(segmentB._children), 2)

    def testAddListInOrder(self):
        segmentA = Pipeline()
        stepA = Step()
        stepB = Step()
        subsegmentC = Segment([Step()])
        segmentA.addListInOrder([stepA, stepB, subsegmentC])

        self.assertEqual(len(segmentA._children), 3)
        self.assertTrue(stepB._isAfter(stepA))
        self.assertTrue(subsegmentC._isAfter(stepB))

    def testComponentsInSegment(self):
        subsegmentA = Segment()
        subsegmentA.add(Step())
        subsegmentA.add(Segment())
        self.assertEqual(len(subsegmentA._children), 2)

        subsegmentB = Segment()
        step = subsegmentB.newStep(name='stepB')
        subSubsegment = subsegmentB.newSegment([Step()])

        self.assertEqual(step.getAttribute('name'), 'stepB')
        self.assertEqual(len(subSubsegment._children), 1)
        self.assertEqual(len(subsegmentB._children), 2)

    def testGetFirstToRunNonflat(self):
        stepA = Step()
        stepB = Step()
        stepC = Step()
        stepD = Step()
        innerSubsegment = Segment([stepA, stepB])
        outerSubsegment = Segment([innerSubsegment, stepC, stepD])

        stepA.before(stepB)
        innerSubsegment.before(stepC)

        first = outerSubsegment._getFirstToRunNonflat()
        self.assertTrue(stepD in first)
        self.assertTrue(innerSubsegment in first)
        self.assertEqual(len(first), 2)

    def testGetFirstToRun(self):
        stepA = Step()
        stepB = Step()
        stepC = Step()
        stepD = Step()
        innerSubsegment = Segment([stepA, stepB])
        outerSubsegment = Segment([innerSubsegment, stepC, stepD])

        stepA.before(stepB)
        innerSubsegment.before(stepC)

        first = outerSubsegment._getFirstToRun()
        self.assertTrue(stepD in first)
        self.assertTrue(stepA in first)
        self.assertEqual(len(first), 2)

    def testGetLastToRunNonflat(self):
        stepA = Step()
        stepB = Step()
        stepC = Step()
        stepD = Step()
        innerSubsegment = Segment([stepC, stepD])
        outerSubsegment = Segment([innerSubsegment, stepA, stepB])

        stepC.before(stepD)
        stepB.before(innerSubsegment)

        last = outerSubsegment._getLastToRunNonflat()
        self.assertTrue(innerSubsegment in last)
        self.assertTrue(stepA in last)
        self.assertEqual(len(last), 2)

    def testGetLastToRun(self):
        stepA = Step()
        stepB = Step()
        stepC = Step()
        stepD = Step()
        innerSubsegment = Segment([stepC, stepD])
        outerSubsegment = Segment([innerSubsegment, stepA, stepB])

        stepC.before(stepD)
        stepB.before(innerSubsegment)

        last = outerSubsegment._getLastToRun()
        self.assertTrue(stepD in last)
        self.assertTrue(stepA in last)
        self.assertEqual(len(last), 2)

    def testGetLastToRunRecursive(self):
        stepA = Step()
        stepB = Step()
        stepC = Step()
        subsegment = Segment([
                stepA,
                Segment([
                        Segment([
                                stepB,
                                stepC
                                ])
                        ])
                ])

        stepC.after(stepB)

        last = subsegment._getLastToRun()
        self.assertTrue(stepC in last)
        self.assertTrue(stepA in last)
        self.assertEqual(len(last), 2)

    def testGetFirstToRunRecursive(self):
        stepA = Step()
        stepB = Step()
        stepC = Step()
        subsegment = Segment([
                Segment([
                        Segment([
                                stepA,
                                stepB
                                ])
                        ]),
                stepC
                ])

        stepA.before(stepB)

        first = subsegment._getFirstToRun()
        self.assertTrue(stepA in first)
        self.assertTrue(stepC in first)
        self.assertEqual(len(first), 2)

    def testContainerFlattenPrerequisites(self):
        segment1 = Pipeline()
        stepA = Step()
        stepB = Step()
        stepC = Step()
        subsegment1 = segment1.newSegment([stepA, stepB])
        segment1.add(stepC)
        stepB.after(stepA)

        segment2 = Pipeline()
        stepD = segment2.newStep()
        stepE = Step()
        stepF = Step()
        subsegment2 = Segment([stepE, stepF])
        subsegment2.after(stepD)
        segment2.add(subsegment2)

        segment2.after(segment1)

        segment2._flattenPrerequisites()

        self.assertTrue(segment2._isAfter(segment1))
        self.assertEqual(
            len(segment2._getPrerequisites()), 
            1)

        self.assertEqual(
            len(stepD._getPrerequisites()), 
            0)

        subsegment2._flattenPrerequisites()

        self.assertTrue(stepE._isAfter(stepD))
        self.assertEqual(
            len(stepE._getPrerequisites()),
                1)

    def testContainerFlattenPending(self):
        segment1 = Pipeline()
        stepA = Step()
        stepB = Step()
        stepC = Step()
        subsegment1 = segment1.newSegment([stepA, stepB])
        segment1.add(stepC)

        segment2 = Pipeline()
        step2 = segment2.newStep()

        stepB.after(stepA)
        segment2.after(segment1)

        segment1._flattenPending()

        self.assertTrue(segment2._isAfter(stepB))
        self.assertTrue(segment2._isAfter(stepC))
        self.assertEqual(
            len(segment2._getPrerequisites()), 
            2)
        self.assertEqual(
            len(step2._getPrerequisites()), 
            0)

    # _AttributableComponent class -----------

    def testSetAttribute(self):
        stepA = Step(name='testname',
                     slots=4)

        self.assertEqual(stepA.getAttribute('name'), 'testname')
        self.assertEqual(stepA.getAttribute('slots'), 4)

        stepB = Step()
        stepB.setAttribute(host='localhost')
        stepB.setAttribute(queue='testqueue', memory='2G')

        self.assertTrue(stepB.getAttribute('name') is not None)

    def testSetInvalidAttribute(self):
        self.assertRaises(
            Exception,
            Step,
            badattribute=7
        )

        A = Pipeline()
        self.assertRaises(
            Exception,
            A.setAttribute,
            badAttribute=7
            )

    def testSetSegmentAttributes(self):
        segmentA = Pipeline(name='testsegment',
                             mail='someone@somewhere.edu')

        self.assertEqual(segmentA.getAttribute('name'), 
                         'testsegment')
        self.assertEqual(segmentA.getAttribute('mail'), 
                         'someone@somewhere.edu')
        
        segmentB = Pipeline()
        self.assertTrue(segmentB.getAttribute('name') is not None)

        segmentB.setAttribute(name='testsegment')
        segmentB.setAttribute(directory='testdirectory',
                               mail='someone@somewhere.edu')

        self.assertEqual(segmentB.getAttribute('name'), 
                              'testsegment')
        self.assertEqual(segmentB.getAttribute('directory'), 
                              'testdirectory')
        self.assertEqual(segmentB.getAttribute('mail'), 
                              'someone@somewhere.edu')

    def testGetUserSetting(self):
        A = Step()
        P = SuperPipe(userSettings={'test':'isSet'})
        S = P.newSegment()

        self.assertIsNone(A.getUserSetting('test'))
        
        S.add(A)
        self.assertEqual(A.getUserSetting('test'), 'isSet')

        S.getAttribute('userSettings')['other'] = 'something'
        self.assertEqual(A.getUserSetting('test'), 'isSet')

        S.getAttribute('userSettings')['test'] = 'isSetAgain'
        self.assertEqual(A.getUserSetting('test'), 'isSetAgain')
                        

    # _JobSettingsComponent ------------------

    def testInheritAttributeFromParent(self):
        P = SuperPipe(directory='this/dir')
        S = Pipeline()
        self.assertIsNone(S.getAttribute('directory'))

        P.add(S)
        self.assertEqual(S.getAttribute('directory'), 'this/dir')

        S.setAttribute(directory='that/dir')
        self.assertEqual(S.getAttribute('directory'), 'that/dir')
        self.assertEqual(P.getAttribute('directory'), 'this/dir')

    def testUniqueDefaultNames(self):
        P = SuperPipe()
        S = P.newSegment()
        self.assertNotEqual(P.getAttribute('name'),
                            S.getAttribute('name'))
        self.assertIsNotNone(P.getAttribute('name'))
        self.assertIsNotNone(S.getAttribute('name'))

    def testSetJobManagerSJM(self):
        P = SuperPipe(jobManager='sjm')
        self.assertEqual(P._getJobManager()._jobManagerType, 'sjm')

    def testSetJobManagerBpipe(self):
        P = SuperPipe(jobManager='bpipe')
        self.assertEqual(P._getJobManager()._jobManagerType, 'bpipe')

    def testSetJobManagerInvalid(self):
        P = SuperPipe()
        self.assertRaises(Exception,
                          P._getJobManager)

        P.setAttribute(jobManager='not_valid')
        self.assertRaises(Exception,
                          P._getJobManager)

    def testJobManagerInheritFromParent(self):
        P = SuperPipe(jobManager='bpipe')
        S = Pipeline()
        P.add(S)

        self.assertEqual(S._getJobManager()._jobManagerType, 'bpipe')
        self.assertEqual(P._getJobManager()._jobManagerType, 'bpipe')
        
        S.setAttribute(jobManager='sjm')

        self.assertEqual(S._getJobManager()._jobManagerType, 'sjm')
        self.assertEqual(P._getJobManager()._jobManagerType, 'bpipe')


    # Step class -----------------------------

    def testStepType(self):
        A = Step()
        self.assertTrue(A._isFlat)
        self.assertFalse(A._isContainer)
        self.assertTrue(A._isContainable)
        self.assertTrue(A._isOrderable)
        self.assertFalse(A._isPipeline)

    def testStepGetFirstLastToRun(self):
        A = Step()
        self.assertEqual(A._getFirstToRun(), [A])
        self.assertEqual(A._getLastToRun(), [A])

    # Segment class --------------------------------------

    def testSegmentType(self):
        A = Segment()
        self.assertFalse(A._isFlat)
        self.assertTrue(A._isContainer)
        self.assertTrue(A._isContainable)
        self.assertTrue(A._isOrderable)
        self.assertFalse(A._isPipeline)

    def testInitializeSegment(self):
        subsegmentA = Segment()

        stepA = Step()
        stepB = Step()
        subsegmentB = Segment([stepA, stepB])

        self.assertEqual(len(subsegmentA._children), 0)
        self.assertEqual(len(subsegmentB._children), 2)

    def testSegmentFlatten(self):
        A = Step()
        B = Step()
        C = Step()
        S = Segment([A, B, C])
        # Cannot flatten unless subsegment has parent
        self.assertRaises(Exception, S._flatten)

    # Pipeline class ----------------------------

    def testPipelineType(self):
        A = Pipeline()
        self.assertTrue(A._isFlat)
        self.assertTrue(A._isContainer)
        self.assertFalse(A._isContainable)
        self.assertTrue(A._isOrderable)
        self.assertTrue(A._isPipeline)

    def testPipelineFlatten(self):
        stepA = Step(name='A')
        stepB = Step(name='B')
        stepB.after(stepA)
        subsegmentA = Segment([stepA, stepB])
        stepC = Step(name='C')
        stepC.after(subsegmentA)
        subsegmentB = Segment([stepC, subsegmentA])
        stepD = Step(name='D')
        subsegmentB.after(stepD)
        # [stepD [[stepA stepB] stepC]]

        segment = Pipeline()
        segment.add(stepD)
        self.assertTrue(segment._isFlat)

        segment.add(subsegmentB)
        self.assertFalse(segment._isFlat)

        segment._flatten()

        self.assertTrue(stepB._isAfter(stepA))
        self.assertTrue(stepC._isAfter(stepB))
        self.assertTrue(stepA._isAfter(stepD))

        self.assertEqual(len(stepA._prerequisiteComponents), 1)
        self.assertEqual(len(stepB._prerequisiteComponents), 1)
        self.assertEqual(len(stepC._prerequisiteComponents), 1)
        self.assertEqual(len(stepD._prerequisiteComponents), 0)

    def testGetSteps(self):
        stepA = Step()
        stepB = Step()
        segment1 = Pipeline([stepA, stepB])
        steps1 = segment1._getSteps()
        self.assertTrue(stepA in steps1)
        self.assertTrue(stepB in steps1)
        self.assertEqual(len(steps1), 2)

        stepC = Step()
        stepD = Step()
        stepE = Step()
        subsegmentA = Segment([stepD, stepE])
        segment2 = Pipeline([stepC, subsegmentA])
        self.assertRaises(Exception,
                          segment2._getSteps)

        segment2._flatten()
        steps2 = segment2._getSteps()
        self.assertTrue(stepC in steps2)
        self.assertTrue(stepD in steps2)
        self.assertTrue(stepE in steps2)
        self.assertEqual(len(steps2), 3)

    def testParent(self):
        S = Pipeline()
        P1 = SuperPipe()
        S._setParent(P1)
        self.assertEqual(S._getParent(),P1)

        P2 = SuperPipe()
        self.assertRaises(
            Exception,
            S._setParent,
            P2
        )

        S._removeParent()
        self.assertIsNone(S._getParent())

    def testPrepareFunction(self):
        # First way to use prepare: assign method using setPrepareFunction()

        def f(pipeline):
            return 3.14

        S = Pipeline()
        S.setPrepareFunction(f)
        # In unfrozen state, function is assigned, serialized version is not
        self.assertIsNotNone(S._prepareFunction)
        self.assertIsNone(S._serialized.get('prepareFunction'))
        self.assertEqual(S.prepare(), 3.14)

        # In frozen state, function is not assigned, serialized version is stored
        S._freeze() 
        self.assertIsNone(S._prepareFunction)
        self.assertIsNotNone(S._serialized.get('prepareFunction'))

        # Deserialize and run when prepare() is called
        self.assertEqual(S.prepare(), 3.14)
        self.assertIsNotNone(S._prepareFunction)
        self.assertIsNone(S._serialized.get('prepareFunction'))


    def testPrepareChildClass(self):
        # Second way to use prepare: subclass and override prepare function
        A = Step()
        S = ChildOfSegment([A])
        self.assertEqual(S.prepare(), 2.72)

        # Freeze should have no effect.
        S._freeze()

        self.assertEqual(S.prepare(), 2.72)
        self.assertTrue(A in S._children)

    def spawnPending(self):
        pass

    # SuperPipe class -------------------------

    def testSuperPipeInit(self):
        S1 = Pipeline()
        S2 = Pipeline()
        S2.after(S1)
        P1 = SuperPipe([S1, S2], name='testpipeline')

        self.assertTrue(S1 in P1._children)
        self.assertTrue(S2 in P1._children)
        self.assertTrue(S2._isAfter(S1))
        self.assertEqual(len(P1._children), 2)
        self.assertEqual(P1.getAttribute('name'), 'testpipeline')

    def testSuperPipeAttributes(self):
        P = SuperPipe()
        P.setAttribute(name='test')
        self.assertEqual(P.getAttribute('name'), 'test')

        self.assertRaises(
            Exception,
            P.setAttribute,
            nonsense='value')

    def testSuperPipeAddRemoveSegment(self):
        S1 = Pipeline()
        S2 = Pipeline()
        P1 = SuperPipe()
        P2 = SuperPipe()

        P1.add(S1)
        self.assertEqual(P1._children, [S1])

        P2.add(S2)
        # Can't add segment that has parent
        self.assertRaises(
            Exception,
            P1.add,
            S1)

        P2._remove(S2)
        self.assertEqual(P2._children, [])

        P1.add(S2)
        self.assertTrue(S1 in P1._children)
        self.assertTrue(S2 in P1._children)
        self.assertEqual(len(P1._children), 2)

    def testNewPipeline(self):
        P = SuperPipe()
        S1 = P.newSegment()
        S2 = P.newSegment()
        self.assertTrue(S1 in P._children)
        self.assertTrue(S2 in P._children)
        self.assertEqual(len(P._children), 2)

    def testAddPipelineList(self):
        S1 = Pipeline()
        S2 = Pipeline()
        S3 = Pipeline()

        P = SuperPipe([S1])
        P.addList([S2, S3])

        self.assertTrue(S1 in P._children)
        self.assertTrue(S2 in P._children)
        self.assertTrue(S3 in P._children)
        self.assertEqual(len(P._children), 3)

    def testGetSuperPipeFile(self):
        # No directory set
        P = SuperPipe(name='testname')
        dir1 = os.getcwd()
        expectedName1 = os.path.join(dir1,'testname'+'.pipsqk')
        self.assertEqual(P._getPipsqkFile(), expectedName1)

        # With directory set
        dir2 = 'some/other/dir'
        P.setAttribute(directory=dir2)
        expectedName2 = os.path.join(dir2,'testname'+'.pipsqk')
        self.assertEqual(P._getPipsqkFile(), expectedName2)

    def testSuperPipeSave(self):
        tempdir = tempfile.mkdtemp()

        P = SuperPipe(
            directory=tempdir,
            name='testpipeline'
        )

        filename = os.path.join(tempdir, 'testpipeline.pipsqk')

        S1a = ChildOfSegment(name='S1') # Already has Segment with steps named A, B, with B.after(A)
        S2a = Pipeline(name='S2')
        S2a.newStep(name='C')
        S2a.after(S1a)
        P.addList([S1a, S2a])

        P.save()

        P2 = SuperPipe.load(filename)

        for segment in P2._children:
            if segment.getAttribute('name') == 'S1':
                S1 = segment
            elif segment.getAttribute('name') == 'S2':
                S2 = segment
            else:
                self.assertTrue(False, 'Couldn\'t recognize segments in reloaded pipeline.')

        S1.prepare() # Normally called by "run"
        AB = S1._children[0]
        for step in AB._children:
            if step.getAttribute('name') == 'A':
                A = step
            elif step.getAttribute('name') == 'B':
                B = step
            else:
                raise Exception('Test needs to be updated. ChildOfSegment class has changed.')

        self.assertEqual(len(S1._children), 1)
        self.assertTrue(B._isAfter(A))

        self.assertEqual(len(S2._children), 1)
        self.assertEqual(S2._children[0].getAttribute('name'), 'C')
        self.assertTrue(S2._isAfter(S1))


    def testSuperPipeGetFirstToRun(self):
        P = SuperPipe()
        S1 = P.newSegment()
        S2 = P.newSegment()

        self.assertTrue(S1 in P._getFirstToRun())
        self.assertTrue(S2 in P._getFirstToRun())
        self.assertEqual(len(P._getFirstToRun()), 2)

        S2.after(S1)
        self.assertTrue(S1 in P._getFirstToRun())
        self.assertEqual(len(P._getFirstToRun()), 1)

    def testPipelineGetParent(self):
        self.assertIsNone(SuperPipe()._getParent())

if __name__ == '__main__':
    unittest.main()
