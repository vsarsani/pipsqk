import copy
import os
import marshal
import pickle
import random
import string
import sys
import types

from pipsqk.job_managers.sjm import SJMJobManager
from pipsqk.job_managers.bpipe import BpipeJobManager

# This module defines the building blocks of a pipeline
#
# SuperPipe: Contains 0 or more Pipelines,
#  corresponds to one .pipsqk file,
#  can be run and restarted as a unit,
#  can accept job manager settings or other settings
#  that propagate to each Pipeline unless overridden
#
# Pipeline: Contains 0 or more Segments or Steps,
#  Corresponds to a "run" or a batch of jobs submitted
#  (e.g. .sjm or .bpipe file).
#  Accepts job manager settings or inherits from SuperPipe, and
#  may be flat or nonflat, depending on whether it contains
#  segments (which are nonflat). Pipeline has a "flatten" 
#  method that is used to translate segments to steps 
#  before generating a file for the job manager.
#  May be ordered after other pipelines, segments, or steps,
#  and before other pipelines.
#  pipelineA.after(componentA) translates to an automatically generated 
#  step in the pipeline that contains componentA that will run after 
#  componentA and will spawn pipelineA by running pipelineA.prepare(). 
#  Therefore, a pipeline may be after multiple components, but
#  all must belong to the same pipeline.
#  pipelineA.before(pipelineB) translates to pipelineB being spawned
#  after the last component in pipelineA runs.
#  The prepare function is user-defined python code that executes
#  at runtime. Therefore, it can be used to examine results from
#  previous pipelines and construct the current pipeline accordingly.
#  It may be defined by (1) subclassing Pipeline and overrding the "prepare"
#  method, or by (2) instantiating a Pipeline and passing a function object
#  to pipeline.setPrepareFunction(). (1) overrides (2).
#
# Segment: A logical structure for grouping steps. This can be
#  before or after other steps or segments in the same pipeline,
#  and before other pipelines. Calling "flatten" on the parent
#  pipeline will remove all segments and replace them with the
#  steps they contained, with equivalent ordering.
#  segmentA.before(segmentB) translates to each of the first
#  steps to run in segmentA being set before each of the last
#  steps to run in segmentB.
#
# Step: Corresponds to a single command run by the job manager.
#  Has attributes required by the job manager, such as qsub
#  settings (memory, time limit, etc.), as well as the command to
#  be executed when that step is run.


class _Helper(object):

    @classmethod
    def randomString(cls, n):

        # Used for generating unique names 
        # when names are not provided

        return ''.join(random.choice(
            string.ascii_lowercase) 
                       for x in range(n))


class _Component(object):

    # This class applies default component type settings.
    # These can be overridden if a component inherits
    # from a subclass that sets these to non-default.

    _isContainable = False
    _isContainer = False
    _isOrderable = False
    _isPipeline = False

    def __init__(self, *args, **kwargs):
        super(_Component, self).__init__()
        self._parent = None
        self._children = []

    def getUserSetting(self, setting):
        # User settings can be inherited from parents. Try self first, then
        # move up the chain.
        try:
            userSettings = self.getAttribute('userSettings')
        except AttributeError:
            userSettings = None
        if userSettings:
            if userSettings.get(setting):
                return userSettings.get(setting)
        # Setting not set. Can we get it from parent?
        if self._getParent():
            return self._getParent().getUserSetting(setting)
        else:
            return None

    def _setParent(self, parent):
        parent.add(self)

    def _removeParent(self):
        self._parent._remove(self)

    def _remove(self, child):
        if not child in self._children:
            raise Exception(
                'Cannot remove child because it does not belong'
                ' to this parent.')
        assert child._getParent() == self
        self._children.remove(child)
        child._parent = None

    def _getParent(self):
        return self._parent


class _OrderableComponent(_Component):

    _isOrderable = True

    def __init__(self, *args, **kwargs):
        super(_OrderableComponent, self).__init__(*args, **kwargs)
        self._prerequisiteComponents = []
        self._pendingComponents = []

    def _verifyOrderable(self, component):
        if not component._isOrderable:
            raise Exception(
                'Non-orderable component. Cannot assign order or check order.')

    def after(self, component):
        self._verifyOrderable(component)
        self._prerequisiteComponents.append(component)
        component._pendingComponents.append(self)
    
    def before(self, component):
        self._verifyOrderable(component)
        component.after(self)

    def _isAfter(self, component):
        self._verifyOrderable(component)
        isAfter = (component in self._prerequisiteComponents)
        assert isAfter == (self in component._pendingComponents)
        return isAfter

    def _isBefore(self, component):
        self._verifyOrderable(component)
        return component._isAfter(self)

    def _unAfter(self, component):
        self._verifyOrderable(component)
        if not ((component in self._prerequisiteComponents) | \
           (self not in component._pendingComponents)):
            raise Exception(
                'Requested to unlink from a component'
                ' that is not a prerequisite.')
        self._prerequisiteComponents.remove(component)
        component._pendingComponents.remove(self)

    def _unBefore(self, component):
        self._verifyOrderable(component)
        component._unAfter(self)

    def afterList(self, componentList):
        for component in componentList:
            self.after(component)

    def beforeList(self, componentList):
        for component in componentList:
            self.before(component)

    def _unAfterList(self, componentList):
        for component in componentList:
            self._unAfter(component)

    def _unBeforeList(self, componentList):
        for component in componentList:
            self._unBefore(component)

    def _getPrerequisites(self):
        return self._prerequisiteComponents

    def _getPending(self):
        return self._pendingComponents


class _ContainableComponent(_Component):

    _isContainable = True

    def __init__(self, *args, **kwargs):

        super(_ContainableComponent, self).__init__(*args, **kwargs)
        self._parent = None

    def _setParent(self, parent):
        if not parent._isContainer:
            raise Exception(
                'Cannot set parent to a component'
                ' that is not a container')
        parent.add(self)

    def _getPipeline(self):
        # Returns the pipeline containing this component
        if not self._getParent():
            return None
        else:
            if self._getParent()._isPipeline:
                return self._getParent()
            else:
                return self._getParent()._getPipeline()
            

class _ContainerComponent(_Component):

    # Containers of Steps and Segments.
    # Although Segments can also belong to parents, they
    # behave differently enough that sharing this class is not useful.

    _isContainer = True

    def __init__(self, componentList=None, *args, **kwargs):
        super(_ContainerComponent, self).__init__(*args, **kwargs)
        if componentList:
            self.addList(componentList)

    def add(self, component):
        if not component._isContainable:
            raise Exception(
                'Cannot add component that is not containable %s'
                % component)
        # Adding a non-flat component to a container makes
        # the container is non-flat
        if not component._isFlat:
            self._isFlat = False
        if component._getParent():
            raise Exception(
                'Component cannot be added to a new container'
                ' because it already has a parent.')
        self._children.append(component)
        component._parent = self
        # Return component as a convenience to allow this:
        #  step = segment.add(Step(...))
        # rather than
        #  step = Step(...); segment.add(step)
        return component

    def addList(self, componentList):
        for component in componentList:
            self.add(component)
            if not component._isFlat:
                self._isFlat = False

    def addListInOrder(self, componentList):
        for i in range(len(componentList)-1):
            componentList[i+1].after(componentList[i])
        self.addList(componentList)

    def _removeList(self, componentList):
        for component in componentList:
            self._remove(component)

    def newStep(self, **kwargs):
        new = Step(**kwargs)
        self.add(new)
        return new

    def newSegment(self, *args):
        new = Segment(*args)
        self.add(new)
        return new

    def _getFirstToRun(self):
        # Returns a list of any Steps that do not have prerequisites
        # in this container
        flatList = []
        for component in self._getFirstToRunNonflat():
            flatList.extend(component._getFirstToRun())
        return flatList

    def _getFirstToRunNonflat(self):
        first = []
        for component in self._children:
            isFirst = True
            for prereq in component._prerequisiteComponents:
                if prereq in self._children:
                    isFirst = False
            if isFirst:
                first.append(component)
        return first

    def _getLastToRun(self):
        # Returns a list of any Steps that do not have pending components
        # in this container
        flatList = []
        for component in self._getLastToRunNonflat():
            flatList.extend(component._getLastToRun())
        return flatList

    def _getLastToRunNonflat(self):
        last = []
        for component in self._children:
            isLast = True
            for pending in component._pendingComponents:
                if pending in self._children:
                    isLast = False
            if isLast:
                last.append(component)
        return last

    def _flatten(self):
        self._flattenPrerequisites()
        self._flattenPending()
        self._flattenContents()

    def _flattenPrerequisites(self):
        for component in self._getFirstToRun():
            component.afterList(self._prerequisiteComponents)
        self._unAfterList(self._prerequisiteComponents)

    def _flattenPending(self):
        for component in self._getLastToRun():
            component.beforeList(self._pendingComponents)
        self._unBeforeList(self._pendingComponents)

    def _flattenContents(self):
        iterComponents = copy.copy(self._children)
        for component in iterComponents:
            component._flatten()


class _AttributableComponent(_Component):

    def __init__(self, *args, **kwargs):
        self.setAttribute(**kwargs)
        super(_AttributableComponent, self).__init__(*args, **kwargs)

    def setAttribute(self, **kwargs):
        self._setAttributeFromDict(kwargs)

    def _setAttributeFromDict(self, attributeDict):
        for attribute in attributeDict:
            if attribute in self.attributes.keys():
                self.attributes[attribute] = attributeDict[attribute]
            else:
                raise Exception(
                    'Attempted to set invalid attribute "%s" to "%s"'
                    % (attribute, attributeDict[attribute])
                    )

    def getAttribute(self, attribute):
        value = self.attributes.get(attribute)
        if not value:
            value=self._getPipeline().getAttribute(attribute)
        return value

class _JobSettingsComponent(_AttributableComponent):

    def __init__(self, *args, **kwargs):
        self.attributes = {
            'name': _Helper.randomString(10), # Never empty
            'directory': None,
            'mail': None,
            'jobManager': None,
            'account': None,
            'host': None,
            'userSettings': {},
        }
        super(_JobSettingsComponent, self).__init__(*args, **kwargs)
        self._jobManager = None

    # Override
    def getAttribute(self, attribute):
        # If not set, return attribute from parent
        value = self.attributes.get(attribute)
        if not value and self._getParent():
            value = self._getParent().getAttribute(attribute)
        return value

    def _getJobManager(self):
        jobManagerSetting = self.getAttribute('jobManager')
        if not jobManagerSetting:
            raise Exception(
                'Job manager is not set.')

        constructors = {
            'sjm': SJMJobManager,
            'bpipe': BpipeJobManager,
        }
        jobManagerConstructor = constructors.get(jobManagerSetting)

        if not jobManagerConstructor:
            raise Exception(
                'Job manager "%s" was not recognized'
                % jobManagerSetting )
        else:
            return jobManagerConstructor()


class Step(_AttributableComponent, _ContainableComponent, _OrderableComponent):

    _isFlat = True

    def __init__(self, **kwargs):
        self.attributes = {
            # Must always be unique non-empty
            'name': _Helper.randomString(10),
            'slots': None,
            'parallel_env': None,
            'memory': None,
            'time': None,
            'queue': None,
            'project': None,
            'export': None,
            'modules': None,
            'directory': None,
            'host': None,
            'sched_options': None,
            'command': None,
            }
        super(Step, self).__init__(**kwargs)

    def _getLastToRun(self):
        return [self]

    def _getFirstToRun(self):
        return [self]

    def _flatten(self):
        # Step is already flat, nothing to do
        pass

    def _remove(self, child):
        raise Exception('Step has no children to remove')

    def __str__(self):
        return self.getAttribute('name')

class Segment(_ContainerComponent, _ContainableComponent, _OrderableComponent):

    _isFlat = False

    def __init__(self, componentList=None):
        # No **kwargs since segment has no attributes
        super(Segment, self).__init__(componentList) 

    def _flatten(self):
        # Transfer children to parent and empty out self
        if not self._getParent():
            raise Exception(
                'Cannot flatten a segment '
                'unless it is assigned to a pipeline')
        super(Segment, self)._flatten()
        for component in copy.copy(self._children):
            self._remove(component)
            component._setParent(self._parent)
        self._removeParent()


class Pipeline(_ContainerComponent, _OrderableComponent, _JobSettingsComponent):

    _isPipeline = True

    def __init__(self, componentList=None, **kwargs):
        self._isFlat = True # This will change if componentList has non-flat members
        self._parent = None

        super(Pipeline, self).__init__(componentList, **kwargs)

        self._serialized = { 'prepareFunction': None }
        self._prepareFunction = None

    # prepare() executes is a user-defined function at pipeline runtime,
    # allowing a pipeline to construct itself using information
    # that was not available when superpipe execution started.
    # The user can call setPrepareFunction() to store a function to
    # be called at runtime. To pickle the Pipeline, _freeze must be called,
    # which will store a serialized function and delete the function object.
    # Later, calling prepare() will deserialize the function and call it.
    # Alternatively, a user can subclass Pipeline and override "prepare"
    # with his own method, which is pickled directly.

    def prepare(self):
        if self._serialized.get('prepareFunction'):
            self._prepareFunction = self._deserializePrepareFunction()
            self._serialized['prepareFunction'] = None

        if self._prepareFunction:
            return self._prepareFunction(self)
        else:
            pass

    def setPrepareFunction(self, function):
        self._prepareFunction = function

    def _freeze(self):
        # Prepare for save
        self._serializePrepareFunction()

    def _serializePrepareFunction(self):
        if self._prepareFunction:
            self._serialized['prepareFunction'] = marshal.dumps(
                self._prepareFunction.func_code)
            self._prepareFunction = None

    def _deserializePrepareFunction(self):
        function = types.FunctionType(
            marshal.loads(self._serialized.get('prepareFunction')), 
            globals())
        return function

    def run(self):
        self.prepare()
        self._verifyJobManager()
        self._flatten()
        self._spawnPendingPipelines()
        self._writeToFile()
        return self._executeJobManager()

    def _writeToFile(self):
        self._verifyJobManager()
        self._getJobManager().writeToFile(self)

    def _executeJobManager(self):
        self._verifyJobManager()
        try:
            retcode = self._getJobManager().executeJobManager(self)
        except OSError as e:
            raise Exception('Error running the job manager "%s". Is it installed? %s' 
                            % (self._getJobManager()._executable, e))
        return retcode

    def _verifyJobManager(self):
        if not self.getAttribute('jobManager'):
            raise Exception(
                'Job manager must be set before running this command.')

    def _getSteps(self):
        assert self._isFlat, 'Must call _flatten() before _getSteps()'
        return self._children

    def _flattenPrerequisites(self):
        # Since pipeline needs to initialize before any steps are run,
        # leave prerequisites linked to the pipeline itself,
        # not to internal components.
        pass

    def _flatten(self):
        super(Pipeline, self)._flatten()
        self._isFlat = True

    def _spawnPendingPipelines(self):
        assert self._isFlat, 'Must call _flatten() before _spawnPendingPipelines()'
        if not self._getParent():
            # No sibling pipelines to start
            return

        # Create a set of pipelines waiting on any part of this pipeline
        pendingPipelines = set()
        for component in self._getPending():
            pendingPipelines.add(component._getPipeline())
        for memberComponent in self._children:
            for component in memberComponent._getPending():
                # TODO this might not be the best in all cases
                if not component._isPipeline:
                    component = component._getPipeline()
                # Don't let the pipeline spawn itself
                if not component == self:
                    pendingPipelines.add(component)
        # TODO fix this. Currently this cheats and sticks every dependency 
        # at the end of a segment. In some cases they could run sooner.
        tail = self._getLastToRun()
        for segment in pendingPipelines:
            step = self.newStep(
                name='psrun_%s' % _Helper.randomString(10),
                command='psrun.py --project %s --pipeline %s --command run' 
                % (segment._getParent()._getPipsqkFile(), segment.getAttribute('name')),
                host='localhost'
            )
            step.afterList(tail)


class SuperPipe(_JobSettingsComponent):

    _fileExtension = 'pipsqk'
    _isPipeline = True
    
    def __init__(self, pipelineList=None, **kwargs):
        super(SuperPipe, self).__init__(**kwargs)
        if pipelineList:
            self.addList(pipelineList)

    def add(self, pipeline):
        if pipeline._getParent():
            raise Exception('Pipeline already belongs to a pipeline. Cannot add it again.')
        self._children.append(pipeline)
        pipeline._parent = self

        # Return component as a convenience to allow this:
        #  pipeline = superpipe.add(Pipeline(...))
        # rather than
        #  pipeline = Pipeline(...); superpipe.add(pipeline)
        return pipeline

    def newSegment(self, *args, **kwargs):
        new = Pipeline(*args, **kwargs)
        self.add(new)
        return new

    def addList(self, pipelineList):
        for pipeline in pipelineList:
            self.add(pipeline)

    def _getPipsqkFile(self):
        if self._fileExtension:
            suffix = '.' + self._fileExtension
        else:
            suffix = ''
        directory = self.getAttribute('directory')
        if not directory:
            directory = os.getcwd()
        return os.path.join(directory, self.getAttribute('name')+suffix)

    def save(self):
        self._freeze()

        directory = os.path.dirname(self._getPipsqkFile())
        if not os.path.exists(directory):
            os.makedirs(directory)
 
        with open(self._getPipsqkFile(),'wb') as f:
            pickle.dump(self, f, protocol=2)

    @classmethod
    def load(cls, filename):
        with open(filename, 'rb') as f:
            try:
                superpipe = pickle.load(f)
            except:
                raise(Exception('Pipsqk could not load the superpipe file "%s". pickle.load failed.' % filename, sys.exc_info()))
        cls._verifyPipeline(superpipe)
        return superpipe

    @classmethod
    def _verifyPipeline(cls, pipeline):
        try: 
            isPipeline = pipeline._isPipeline
        except NameError:
            isPipeline = False
        if not (isPipeline):
            raise Exception(
                'Object was not recognized as a valid pipeline'
            )

    def _freeze(self):
        # Prepare for save
        for pipeline in self._children:
            pipeline._freeze()

    def run(self):
        self.save()
        retcodes = []
        for pipeline in self._getFirstToRun():
            retcode = pipeline.run()
            retcodes.append(retcode)
        return retcodes

    def _getFirstToRun(self):
        first = []
        for pipeline in self._children:
            if not pipeline._prerequisiteComponents:
                first.append(pipeline)
        return first

    def _getParent(self):
        return None

    # Override
    def _setParent(self):
        raise Exception("Cannot set parent for a SuperPipe")
