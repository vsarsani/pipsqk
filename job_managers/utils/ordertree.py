class OrderTree(object):
    def __init__(self, itemList):
        self._rootNode = OrderTree.Node()
        self._listToTree(itemList, self._rootNode)

    def render(self):
        return self._rootNode.render()

    @classmethod
    def _listToTree(cls, itemList, node):
        # If we get an object, wrap it in a list for compatibility
        if not hasattr(itemList, '__iter__'):
            itemList = [itemList]

        assert len(itemList) > 0

        if len(itemList) == 1:
            cls._terminalNode(itemList, node)            
            return

        # If not all steps are related through dependencies,
        # Break apart, and run concurrently

        itemGroups = cls._separateUnrelatedSteps(itemList)

        if len(itemGroups) > 1:
            cls._concurrentNode(itemGroups, node)
            return
        else:
            itemList = itemGroups[0]

        # All items are related through dependencies
        # Break off nodes that are not dependent on others, along with
        # any linear path above them to the first branch point.
        # lowerBranches is a list of nodes from one or more branches
        # which will be broken up by separateUnrelatedSteps on the next pass.

        lowerBranches = cls._removeLowerBranchMembers(itemList)

        if len(lowerBranches) == 0:
            raise Exception('Order dependency loop. Cannot find any next steps to run.')

        if len(itemList) == 0:
            # All items are related, and all came off as lower branches
            # Therefore one linear branch is left. Run it in order
            
            cls._consecutiveNode(lowerBranches, node)
            return

        # Branches first, then trunk
        cls._consecutiveNode([lowerBranches, itemList], node)
        return

    @classmethod
    def _terminalNode(cls, itemList, node):
        assert len(itemList) == 1

        node.op = None
        node.terms = itemList

    @classmethod
    def _consecutiveNode(cls, groups, node):

        assert len(groups) > 1

        node.op = '+'
        for group in groups:
            child = OrderTree.Node()
            node.terms.append(child)
            cls._listToTree(group, child)


    @classmethod
    def _concurrentNode(cls, groups, node):

        assert len(groups) > 1

        node.op = ','
        for group in groups:
            child = OrderTree.Node()
            node.terms.append(child)
            cls._listToTree(group, child)

    @classmethod
    def _separateUnrelatedSteps(cls, allItems):
        # copy to itemList so that allItems can remain unchanged
        itemList = []
        itemList.extend(allItems)

        separatedLists = []
        while len(itemList) > 0:
            newList = [itemList.pop()]
            addedSomethingThisLoop = True
            while addedSomethingThisLoop == True:
                addedSomethingThisLoop = False
                
                # If A in newList depends on B in itemList
                # transfer B to newList
                for item in newList:
                    for prereq in item._getPrerequisites():
                        if prereq in itemList:
                            itemList.remove(prereq)
                            newList.append(prereq)
                            addedSomethingThisLoop = True

                # If A in newList is depended on by B in itemList
                # transfer B to newList
                for item in itemList:
                    for prereq in item._getPrerequisites():
                        if prereq in newList:
                            if item in itemList:
                                itemList.remove(item)
                                newList.append(item)
                                addedSomethingThisLoop = True

            separatedLists.append(newList)
        return separatedLists

    @classmethod
    def _removeLowerBranchMembers(cls, itemList):
        # Finds all nodes without prerequisits, plus
        # the linear path up from these items. Removes
        # them from itemList and returns them as branch

        # allItems stays constant while we pop from itemList
        allItems = [] 
        allItems.extend(itemList) 

        leaves = cls._removeItemsWithNoPrerequisites(itemList)

        branches = []
        for leaf in leaves:
            branches.extend(cls._removeBranch(
                    leaf, itemList, allItems))
        
        assert len(itemList) + len(branches) == len(allItems)

        return branches

    @classmethod
    def _removeBranch(cls, leaf, remainingItems, allItems):
        # Given leaf, extend branch up to first fork.
        # Remove branch members from remainingItems
        # and return branch
        #
        # allItems cond contain remainingItems plus items 
        # from branches that were previoiusly deleted from 
        # remainingItems, but we still use them to identify forks

        assert leaf in allItems
        assert leaf not in remainingItems

        for item in remainingItems:
            assert item in allItems

        branch = [leaf] #seed and extend

        keepGoing = True
        while keepGoing:
            candidates = []
            for item in allItems:
                if item not in remainingItems:
                    continue
                if item._isAfter(leaf):
                    # This might extend the linear branch,
                    # but only if it isn't a fork
                    remainingItems.remove(item)
                    candidates.append(item)
            if len(candidates) != 1:
                # Not a linear branch, because it forks upstream,
                # Or nothing upstream was found.
                # Put back the upstream forks if any
                remainingItems.extend(candidates)
                break
            candidate = candidates.pop() # There is only one
            for item in allItems:
                if candidate._isAfter(item) and item not in branch:
                    # Not a linear branch, because it forks downstream.
                    # Put it back.
                    remainingItems.append(candidate)
                    keepGoing = False
                    
            if not keepGoing:
                break
            # Move up a step and repeat
            leaf = candidate
            candidate = None
            branch.append(leaf)
        return branch

    @classmethod
    def _removeItemsWithNoPrerequisites(cls, itemList):
        # allItems remains constant while items are removed from itemList
        allItems = []
        allItems.extend(itemList)

        itemsWithNoPrereqs = []
        for item in allItems:
            hasPrereq = False
            for prerequisite in item._getPrerequisites():
                if prerequisite in allItems:
                    hasPrereq = True
                    break
            if not hasPrereq:
                itemList.remove(item)
                itemsWithNoPrereqs.append(item)

        return itemsWithNoPrereqs

    class Node(object):
        def __init__(self):

            self.op = None # ','= concurrent, '+'= consecutive, 'None' = terminal node
            self.terms = [] 
            # terms are Nodes; except leaves, which are the object being ordered
            
        def render(self):

            if self.op is None:
                assert len(self.terms) == 1
                return str(self.terms[0])
            else:
                rendered_terms = []
                for term in self.terms:
                    rendered_terms.append(term.render())
                return '[%s]' % self.op.join(rendered_terms)

        def __str__(self):
            return "{'%s',%s}" % (self.op, self.terms)
