from abstract import AbstractJobManager

import subprocess

class SJMJobManager(AbstractJobManager):

    _fileExtension = 'sjm'
    _executable = 'sjm'
    _jobManagerType = 'sjm'

    def executeJobManager(self, segment):
        cmd = [self._executable, self._getFileName(segment)]
        return subprocess.call(cmd)

    def _renderSegmentToText(self, segment):
        text = ''
        if segment.getAttribute('directory'):
            text += 'log_dir %s\n' % segment.getAttribute('directory')
        for step in segment._getSteps():
            text += self._renderStepToText(step)
        for step in segment._getSteps():
            text += self._renderOrderToText(step)
        return text

    def _renderStepToText(self, step):
        text = "job_begin\n"

        for line in self._renderStepBody(step):
            text += '\t'+line

        text += "job_end\n"
        return text

    def _renderStepBody(self, step):
        text = []
        text.append('name %s\n' % step.getAttribute('name'))
        if step.getAttribute('command'):
            text.append('cmd_begin\n')
            text.append('\t%s\n' % step.getAttribute('command'))
            text.append('cmd_end\n')
        if step.getAttribute('host'):
            text.append('host %s\n' % step.getAttribute('host'))
        if step.getAttribute('directory'):
            text.append('directory %s\n' % step.getAttribute('directory'))
        if step.getAttribute('modules'):
            for module in step.getAttribute('modules'):
                text.append('module %s\n' % module)
        if step.getAttribute('memory'):
            text.append('memory %s\n' % step.getAttribute('memory'))
        if step.getAttribute('project'):
            text.append('memory %s\n' % step.getAttribute('memory'))
        if step.getAttribute('account'):
            text.append('sched_options -A %s\n' % step.getAttribute('account'))
        if step.getAttribute('host'):
            text.append('host %s\n' % step.getAttribute('host'))
        return text

    def _renderOrderToText(self, step):
        text = ''
        for prerequisiteStep in step._prerequisiteComponents:
            text += "order %s after %s\n" % (
                step.getAttribute('name'), 
                prerequisiteStep.getAttribute('name'))
        return text
