from abstract import AbstractJobManager
import os
from utils.ordertree import OrderTree

import subprocess

class BpipeJobManager(AbstractJobManager):

    _fileExtension = 'bpipe'
    _executable = 'bpipe'
    _jobManagerType = 'bpipe'

    def executeJobManager(self, segment):
        cmd = [self._executable, self._getFileName(segment)]
        with open(os.devnull, 'w') as devnull:
            retcode = subprocess.call(
                cmd, 
                cwd = segment.getAttribute('directory'),
                stderr=devnull,
                stdout=devnull,
            )
        return retcode

    def _renderSegmentToText(self, segment):
        text = ''
        for step in segment._getSteps():
            text += self._renderStepToText(step)
        text += 'Bpipe.run { %s }' % self._renderRunOrder(segment)
        return text

    def _renderRunOrder(self, segment):
        tree = OrderTree(segment._getSteps())
        return tree.render()

    def _renderStepToText(self, step):
        text = ''
        text += '%s = {\n' %  step.getAttribute('name')
        text += 'exec "%s"\n' % step.getAttribute('command')
        text += '}\n'
        return text
